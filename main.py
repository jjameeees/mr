import requests
import logging
from time import sleep
import telegram
from telegram.error import NetworkError, Unauthorized
import json
import os

def resp():
    link='https://gitlab.com/api/v4/projects/32555937/merge_requests?state=opened'
    token=os.getenv('GL_TOKEN')

    r=requests.get(link, headers={"Authorization":token})
    data = json.loads(r.content)
    username=(data[0]['author']['username'])  # Or `print(data['two'])` in Python 3
    description=(data[0]['description'])  
    web_url=(data[0]['web_url'])
    responce = username + ' "' + description + '" ' + web_url
    print(responce)
    return responce


global UPDATE_ID
UPDATE_ID = None

def echo(bot):
    """Эхо сообщение отправленное пользователем."""
    global UPDATE_ID
    # Запрашиваем обновление после последнего update_id
    for update in bot.get_updates(offset=UPDATE_ID, timeout=10):
        UPDATE_ID = update.update_id + 1

        # бот может получать обновления без сообщений
        if update.message:
            # не все сообщения содержат текст
            if update.message.text:
                # Ответ на сообщение
                update.message.reply_text(f'MR: {resp()}')


if __name__ == '__main__':
    """Запускаем бота."""

    # Токен авторизации бота Telegram
    tokent = os.getenv('TG_TOKEN')
    #print(tokent) ##########################
    bot = telegram.Bot(tokent)

    # получаем первый ожидающий `update_id`
    try:
        UPDATE_ID = bot.get_updates()[0].update_id
    except IndexError:
        UPDATE_ID = None

    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    while True:
        try:
            echo(bot)
        except NetworkError:
            sleep(10000)
        except Unauthorized:
            # Пользователь удалил или заблокировал бота.
            UPDATE_ID += 1
